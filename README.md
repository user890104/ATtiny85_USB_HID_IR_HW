ATtiny85_USB_HID_IR_HW
======================

Firmware for ATtiny85 that accepts commands over USB HID and sends them as Infrared messages.

It is configured for Digispark board.

http://digistump.com/products/1

PB0 = IR transmitter
PB1 = LED
PB2 = Unused
PB3 = USB DATA -
PB4 = USB DATA +
PB5 = /RESET

AVR Memory Usage
----------------
Device: attiny85

Program:    2550 bytes (31.1% Full)
(.text + .data + .bootloader)

Data:        135 bytes (26.4% Full)
(.data + .bss + .noinit)
