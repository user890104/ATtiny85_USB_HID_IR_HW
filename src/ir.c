#include <stdint.h>

#include "usbdrv.h"

static inline void processNextCommand(void);
static inline void processSingle(void);
static inline void processSequence(void);

#define IR_ON() (PORTB |= _BV(PB0))
#define IR_OFF() (PORTB &= ~_BV(PB0))

#define TIMER_ON(PS) (TCCR0B = (TCCR0B & 0xF8) | (PS & 0x07))
#define TIMER_OFF() (TCCR0B &= ~(_BV(CS02) | _BV(CS01) | _BV(CS00)))
#define TIMER_SET(cnt) (OCR0A = (cnt))

extern uint8_t sendPacket;
extern uchar usbBuffer[64];

static uint8_t bit, bytes, phase, end, mode, pos, reverse, t1, t2_off, t2_on;

register unsigned char timerBusy asm("r2");
/*
register unsigned char bytes asm("r3");
register unsigned char phase asm("r4");
register unsigned char end asm("r5");
register unsigned char sequence asm("r6");
register unsigned char pos asm("r7");
*/

void irPrepare(void) {
    DDRB |= _BV(DDB0); // IR Transmitter
    TCCR0A |= _BV(WGM01); // CTC
    TIMSK |= _BV(OCIE0A); // Compare interrupt
}

void irStart(void) {
    if (usbBuffer[0] != 0x01) {
        sendPacket = 0;
        return;
    }

    PORTB |= _BV(PB1);

    mode = 0;
    pos = 1;
    TIMER_SET(1);
    TIMER_ON(0b101);
}

void irProcess(void) {
    if (timerBusy) {
        return;
    }

    timerBusy = 1;

    processCommand:

    if (mode == 0) {
        processNextCommand();
    }

    if (mode == 1) {
        processSingle();

        if (mode == 0) {
            goto processCommand;
        }
    }

    if (mode == 2) {
        processSequence();

        if (mode == 0) {
            goto processCommand;
        }
    }
}

static inline void processNextCommand(void) {
    if (usbBuffer[pos] == 0) {
        TIMER_OFF();
        IR_OFF();
        sendPacket = 0;
        PORTB &= ~_BV(PB1);
        return;
    }

    phase = 0;

    uint8_t currByte = usbBuffer[pos++];
    TIMER_ON(currByte);

    if (!(currByte & 0x80)) {
        // single
        mode = 1;

        TIMER_SET(usbBuffer[pos++]);

        (currByte & 0x40) ? IR_ON() : IR_OFF();

        return;
    }

    // sequence
    mode = 2;
    reverse = currByte & 0x40;
    t1 = usbBuffer[pos++];
    t2_off = usbBuffer[pos++];
    t2_on = usbBuffer[pos++];
    bytes = usbBuffer[pos++];

    bit = 0;
    end = 0;
}

static inline void processSingle(void) {
    if (phase == 0) {
        phase = 1;
        return;
    }

    mode = 0;
}

static inline void processSequence(void) {
    if (phase == 0) {
        TIMER_SET(t1);
        IR_ON();
        phase = 1;
        return;
    }

    phase = 0;

    IR_OFF();

    if (end == 1) {
        mode = 0;
        return;
    }

    TIMER_SET((usbBuffer[pos] & _BV(reverse ? 7 - bit : bit)) ? t2_on : t2_off);

    ++bit;

    if (bit > 7) {
        bit = 0;
        ++pos;
        --bytes;

        if (!bytes) {
            end = 1;
        }
    }
}

ISR(TIMER0_COMPA_vect) {
    timerBusy = 0;
}
