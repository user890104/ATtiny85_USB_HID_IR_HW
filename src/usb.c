#include <avr/wdt.h>
#include <avr/pgmspace.h> // required by usbdrv.h

#include <util/delay.h> // for _delay_ms()

#include "usbdrv.h"

extern usbMsgPtr_t usbMsgPtr;
extern uint8_t sendPacket;

// USB report descriptor
/*
PROGMEM const char usbHidReportDescriptor[20] = {
    0x05, 0x0c,       // USAGE_PAGE (Consumer Devices)
    0x09, 0x01,       // USAGE (Consumer Control)
    0xa1, 0x01,       // COLLECTION (Application)
    0x09, 0x01,       //   USAGE (Consumer Control)
    0x15, 0x00,       //   LOGICAL_MINIMUM (0)
    0x26, 0xff, 0x00, //   LOGICAL_MAXIMUM (255)
    0x75, 0x08,       //   REPORT_SIZE (8)
    0x95, 0x40,       //   REPORT_COUNT (64)
    0xb1, 0x02,       //   FEATURE (Data,Var,Abs)
    0xc0              // END_COLLECTION
};
*/
PROGMEM const char usbHidReportDescriptor[22] = {
    0x05, 0x0c,       // USAGE_PAGE (Consumer Devices)
    0x0a, 0x05, 0x01, // USAGE (Room Temperature)
    0xa1, 0x01,       // COLLECTION (Application)
    0x0a, 0x05, 0x01, //   USAGE (Room Temperature)
    0x15, 0x00,       //   LOGICAL_MINIMUM (0)
    0x26, 0xff, 0x00, //   LOGICAL_MAXIMUM (255)
    0x75, 0x08,       //   REPORT_SIZE (8)
    0x95, 0x40,       //   REPORT_COUNT (64)
    0xb1, 0x02,       //   FEATURE (Data,Var,Abs)
    0xc0              // END_COLLECTION
};


// USB report data
uchar usbBuffer[64];
static uchar currentPosition, bytesRemaining;

void usbPrepare(void) {
    DDRB |= _BV(DDB1); // LED

    uchar i;

    // enforce re-enumeration, do this while interrupts are disabled!
    usbDeviceDisconnect();
    i = 0;

    // fake USB disconnect for > 250 ms
    while (--i) {
        wdt_reset();
        _delay_ms(1);
    }

    usbDeviceConnect();

    usbInit();
}

usbMsgLen_t usbFunctionSetup(uchar setupData[8]) {
    usbRequest_t *rq = (void *)setupData;   // cast to structured data for parsing

    // HID class request
    if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) {
        if (rq->bRequest == USBRQ_HID_SET_REPORT) {
            // wValue: ReportType (highbyte), ReportID (lowbyte)
            // since we have only one report type, we can ignore the report-ID
            currentPosition = 0;                // initialize position index
            bytesRemaining = rq->wLength.word;  // store the amount of data requested

            if (bytesRemaining > sizeof(usbBuffer)) {  // limit to buffer size
                bytesRemaining = sizeof(usbBuffer);
            }

            return USB_NO_MSG;  // tell driver to use usbFunctionWrite()
        }

        if (rq->bRequest == USBRQ_HID_GET_REPORT) {
            usbMsgLen_t len = 64;               // we return up to 64 bytes

            if (len > rq->wLength.word) {       // if the host requests less than we have
                len = rq->wLength.word;         // return only the amount requested
            }

            usbMsgPtr = (usbMsgPtr_t)usbBuffer;    // tell driver where the buffer starts

            return len;                         // tell driver how many bytes to send
        }
    }

    return 0;
}

uchar usbFunctionWrite(uchar *data, uchar len) {
    uchar i;

    if (len > bytesRemaining) { // if this is the last incomplete chunk
        len = bytesRemaining;   // limit to the amount we can store
    }

    bytesRemaining -= len;

    for (i = 0; i < len; i++) {
        usbBuffer[currentPosition++] = data[i];
    }

    uchar isFinished = bytesRemaining == 0; // return 1 if we have all data

    if (isFinished) {
        if (sendPacket == 0) {
            sendPacket = 1;
        }
    }

    return isFinished;
}
