/*
 * This project is based on hid-data, provided as an example of V-USB usage
 * This project uses code from V-USB (http://www.obdev.at/products/vusb/)
 *
 * This project is configured for ATtiny85 running on 16.5 MHz PLL clock
 * The pinout is as follows:
 * PB0 = Transmitter
 * PB1 = LED
 * PB2 = Unused
 * PB3 = USB DATA -
 * PB4 = USB DATA +
 * PB5 = /RESET
 *
 * You can find the schematic for the board at http://digistump.com/products/1
 */

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h> // for sei()

#include <avr/pgmspace.h> // required by usbdrv.h

#include "usbdrv.h"

#include <usb.h>
#include <ir.h>

volatile uint8_t sendPacket = 0;
// 0 - idle
// 1 - unsent
// 2 - sending

int main(void) {
    wdt_enable(WDTO_1S);
    /*
    Even if you don't use the watchdog, turn it off here. On newer devices,
    the status of the watchdog (on/off, period) is PRESERVED OVER RESET!

    RESET status: all port bits are inputs without pull-up.
    That's the way we need D+ and D-. Therefore we don't need any
    additional hardware initialization.
    */

    irPrepare();
    usbPrepare();
    sei();

    // main event loop
    while (1) {
        wdt_reset();

        if (sendPacket == 1) {
            sendPacket = 2;
            irStart();
        }

        irProcess();
        usbPoll();
    }

    return 0;
}
