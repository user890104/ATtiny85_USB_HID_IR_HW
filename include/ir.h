#ifndef INCLUDE_IR_H_
#define INCLUDE_IR_H_

void irPrepare(void);
void irStart(void);
void irProcess(void);

#endif /* INCLUDE_IR_H_ */
